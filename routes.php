<?php

require_once __DIR__ . '/dbHandler.php';
require_once __DIR__ . '/mail_util.php';


$app->get( '/', function ( $request, $response, $args ) {
	$response->getBody()->write('Nothing to show here');
} );

$app->get( '/cron', function ( $request, $response, $args ) {
	$dbh  = new DbHandler();
	$data = $dbh->getUsers();
	$today = date("Y-m-d");
	print_r($today);
	while ($row = mysqli_fetch_row($data)) {
		if($today == $row[5]){
			send_mail($row);
			print_r("now delete");
			$dbh->deleteUser($row);
		}
	}
});

$app->post( '/addUser', function ( $request, $response, $args ) {
	$data = $request->getParsedBody();

	$dbh = new DbHandler();
	$isUser = $dbh->checkIfUserExists($data['userId']);

	$dbh->insertOrUpdateUser( $data, $isUser);
});