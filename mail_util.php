<?php

require 'vendor/autoload.php';

function send_mail($row) {

	$receivers = json_decode($row[2]);

	$body = $row[3];
	$subject = $row[4];

	$mail = new PHPMailer(true);

	try {

		$mail->isSMTP();
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => MAIL_SMTP_PEER_VERIFY,
				'verify_peer_name' => MAIL_SMTP_PEER_VERIFY
			)
		);

		$mail->Host = MAIL_SMTP_HOST;
		$mail->SMTPAuth = MAIL_SMTP_AUTH;
		$mail->Username = MAIL_SMTP_USER;
		$mail->Password = MAIL_SMTP_PWD;
		$mail->SMTPSecure = MAIL_SMTP_SECURE;
		$mail->Port = MAIL_SMTP_PORT;

		$mail->From = MAIL_FROM;
		$mail->FromName = MAIL_FROM_NAME;

		foreach ($receivers as $value) {
			$mail->addAddress($value);
		}

		$mail->isHTML(true);
		$mail->CharSet = 'utf-8';

		$mail->Subject = $subject;
		$mail->Body    = $body;

		$mail->SMTPDebug = 0;

		$result = $mail->send();

		return true;

	} catch(phpmailerException $e) {

		print_r('Could not send mail!');
		return false;
	}
}