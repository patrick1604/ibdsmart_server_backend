<?php

class DbHandler {

	private $conn;

	function __construct() {
		require_once 'dbConnect.php';
		// opening db connection
		$db         = new dbConnect();
		$this->conn = $db->connect();
	}

	public function getUsers() {
		return mysqli_query($this->conn,"SELECT * FROM users");
	}

	public function deleteUser($row){

		$sql = "DELETE FROM users WHERE (userId = ?)";

		if($stmt = mysqli_prepare($this->conn, $sql)){
			mysqli_stmt_bind_param($stmt, "s", $row[1]);
		}
		mysqli_stmt_execute($stmt);
	}

	public function checkIfUserExists($id) {

		$sql = "SELECT * FROM users WHERE (userId = ?)";

		if($stmt = mysqli_prepare($this->conn, $sql)){
			mysqli_stmt_bind_param($stmt, "s", $id);
		}
		mysqli_stmt_execute($stmt);

		if($stmt->get_result()->num_rows == 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function insertOrUpdateUser($data, $insert) {

		print_r($insert);
			if(!$insert){

				$sql = "UPDATE  users SET recipients = ?, userId = ?, body = ?, subject = ?, sendDate = ? ".
				       "WHERE userID = ?";
				if($stmt = mysqli_prepare($this->conn, $sql)){
					mysqli_stmt_bind_param($stmt, "ssssss", json_encode($data['recipients']), $data['userId'], $data['body'],
						$data['subject'], $data['sendDate'], $data['userId']);
				}
			}
			else{
				$sql = "INSERT INTO users(recipients, userId, body, subject, sendDate)".
				       " VALUES (?, ?, ? ,?, ?);";

				if($stmt = mysqli_prepare($this->conn, $sql)){
					mysqli_stmt_bind_param($stmt, "sssss", json_encode($data['recipients']), $data['userId'], $data['body'],
						$data['subject'], $data['sendDate']);
				}
			}
		mysqli_stmt_execute($stmt);
	}
}
