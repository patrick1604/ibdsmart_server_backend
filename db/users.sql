--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userId` varchar(255) NOT NULL DEFAULT '',
  `recipients` varchar(255) NOT NULL DEFAULT '',
  `body` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) DEFAULT '',
  `sendDate` DATE NOT NULL DEFAULT '2018-01-01',
  PRIMARY KEY (uid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores user data.';

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`uid`, `userId`, `recipients`, `body`, `subject`, `sendDate`) VALUES
(0, 'fb3f006a-3e4b-11e8-b467-0ed5f89f718b', 'patrick.frohmann@codefluegel.com', 'Hi', 'Alert', '2018-01-01');


